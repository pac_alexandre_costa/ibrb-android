package pac.com.ibrbbiblioteca.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.model.Autor
import kotlinx.android.synthetic.main.fragment_autor.*

/**
 * Created by alexandre on 26/04/18.
 */
class AutorFragment:android.app.Fragment() {
    var autor: Autor?=null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater?.inflate(R.layout.fragment_autor, container, false)!!
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        autor?.let{
            detail_autor_nome.text = it.nome
        }

        link_see_others_books.setOnClickListener(object: View.OnClickListener{
            override fun onClick(view: View?) {
                var seeAuthorsBooks: String = resources.getString(R.string.see_books_author_broadcast)
                var intenteSeeAuthorsBooks: Intent = Intent(seeAuthorsBooks)
                intenteSeeAuthorsBooks.putExtra(
                        resources.getString(R.string.author_selected)
                        , autor)
                LocalBroadcastManager
                        .getInstance(activity)
                        .sendBroadcast(intenteSeeAuthorsBooks)
            }
        })


    }
}