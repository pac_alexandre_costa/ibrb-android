package pac.com.ibrbbiblioteca.fragment

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pac.com.ibrbbiblioteca.model.Livro
import kotlinx.android.synthetic.main.fragment_livro.*
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.util.ImageUtil

/**
 * Created by alexandre on 26/04/18.
 */
class LivroFragment : android.app.Fragment() {
    var livro : Livro?=null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        savedInstanceState?.let{
            Log.i(this.javaClass.canonicalName, "Recuperou estado")
        }
    }

    override fun onCreateView(inflater: LayoutInflater?
                              , container: ViewGroup?
                              , savedInstanceState: Bundle?): View {
        return inflater?.inflate(R.layout.fragment_livro, container, false)!!
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var livroInstanceState : Livro? = savedInstanceState?.getSerializable("livro") as Livro?
        livro = if(livro==null) livroInstanceState else livro
        livro?.let{



            detail_livro_ano?.text = it.ano.toString()
            detail_livro_autores?.text = it.autores()
            detail_livro_descricao?.text = it.descricao
            detail_livro_titulo?.text = it.titulo

            if(it.imagem!=null){
                var decodedByte: Bitmap = ImageUtil().toBitmap(it.imagem)
                detail_livro_imagem.visibility = View.VISIBLE
                detail_livro_imagem.setImageBitmap(decodedByte)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        Log.i(this.javaClass.canonicalName, "Salvou estado")
        outState?.putSerializable("livro", livro)
    }






}