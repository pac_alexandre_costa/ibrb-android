package pac.com.ibrbbiblioteca.fragment

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.text.TextUtils
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.activity_home.*
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.model.conversation.AccountRequest
import pac.com.ibrbbiblioteca.model.conversation.AccountResponse
import pac.com.ibrbbiblioteca.restapi.RetrofitInit
import pac.com.ibrbbiblioteca.util.JwtUtil
import pac.com.ibrbbiblioteca.util.UserUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response



/**
 * Created by alexandre on 10/05/18.
 */
class LoginFragment() : android.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_login, container,false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnLogin.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                tentarLogin()
            }
        })



        edtPassword.setOnEditorActionListener(object : TextView.OnEditorActionListener{
            override fun onEditorAction(textView: TextView?, id: Int, keyEvent: KeyEvent?): Boolean {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    tentarLogin()
                    return true
                }
                return false
            }
        })

        link_signup.setOnClickListener(object: View.OnClickListener{
            override fun onClick(view: View?) {
                var signupRequestBroadcastTag : String = resources.getString(R.string.signup_request_broadcast)
                var signupRequestBroadcast : Intent = Intent(signupRequestBroadcastTag)
                LocalBroadcastManager
                        .getInstance(activity)
                        .sendBroadcast(signupRequestBroadcast)
            }
        })
    }

    fun tentarLogin(){

        var focusView: View? = null
        var cancel : Boolean = false
        edtEmail.error = null
        edtPassword.error =null

        var email:String = edtEmail.text.toString()
        var password:String = edtPassword.text.toString()

        if(TextUtils.isEmpty(password)){
            edtPassword.setError(getString(R.string.error_field_required))
            focusView = edtPassword
            cancel = true
        }


        if(cancel){
            focusView?.requestFocus()
        }else{
            exibirProgresso(true)
            realizarLogin(email,password)
        }
    }

    fun exibirProgresso(exibir:Boolean){
        login_progress.visibility = if (exibir) View.VISIBLE else View.GONE
        login_form.visibility = if (exibir) View.GONE else View.VISIBLE
    }

    fun realizarLogin(email:String, password:String){

        var loginRequest : AccountRequest = AccountRequest("", email, password)

        var loginCall = RetrofitInit().
                bibliotecaApi().
                login(loginRequest)
        loginCall.enqueue(object : Callback<AccountResponse>{
            override fun onFailure(call: Call<AccountResponse>?
                                   , t: Throwable?) {
                exibirProgresso(false);
                var v:View = main_layout
                Snackbar.make(v, R.string.service_unavailable, Snackbar.LENGTH_LONG).show();

            }


            override fun onResponse(call: Call<AccountResponse>?
                                    , response: Response<AccountResponse>?) {
                if(response?.code()==200){
                    var accountResponse:AccountResponse = response?.body() as AccountResponse
                    JwtUtil().setJwt(accountResponse.token, activity)
                    JwtUtil().setJwt(accountResponse.token, activity)
                    UserUtil().storeEmailPassword(activity, email, password)
                    exibirProgresso(false)
                    var loginBroadcast : String = resources.getString(R.string.login_broadcast)
                    LocalBroadcastManager
                            .getInstance(activity)
                            .sendBroadcast(Intent(loginBroadcast))

                }else{
                    var v:View = login_main_layout
                    Snackbar.make(v, R.string.login_failure_invalid_userpassword, Snackbar.LENGTH_LONG ).show()
                    exibirProgresso(false)
                }

            }



        })
    }


}