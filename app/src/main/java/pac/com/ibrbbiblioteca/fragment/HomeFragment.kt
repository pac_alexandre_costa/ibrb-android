package pac.com.ibrbbiblioteca.fragment

import android.app.Fragment
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pac.com.ibrbbiblioteca.R
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * Created by paulo on 04/05/2018.
 */
//Exibe os livros favoritos de cada usuario
class HomeFragment : android.app.Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_home,  container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState==null) {

            var fragTop10 : Fragment? = childFragmentManager
                    .findFragmentById(R.id.home_content_top10)

            fragTop10 = if (fragTop10==null)Top10Fragment()else fragTop10

            var fragFavoritos : Fragment? = childFragmentManager
                    .findFragmentById(R.id.home_content_favoritos)

            fragFavoritos = if (fragFavoritos==null) FavoritosFragment() else fragFavoritos


            childFragmentManager
                    .beginTransaction()
                    .replace(R.id.home_content_top10, fragTop10)
                    .replace(R.id.home_content_favoritos, fragFavoritos)
                    .commit()
        }


    }
}