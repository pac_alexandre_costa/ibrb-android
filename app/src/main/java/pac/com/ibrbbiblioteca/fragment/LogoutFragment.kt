package pac.com.ibrbbiblioteca.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.util.JwtUtil
import pac.com.ibrbbiblioteca.util.UserUtil

/**
 * Created by paulo on 15/05/2018.
 */
class LogoutFragment : android.app.Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        JwtUtil().clearJwt(this.activity)
        UserUtil().clearEmailPassword(this.activity)

        var logoutBroadcast : String = resources.getString(R.string.logout_broadcast)
        LocalBroadcastManager
                .getInstance(this.activity)
                .sendBroadcast(Intent(logoutBroadcast))

    }

}