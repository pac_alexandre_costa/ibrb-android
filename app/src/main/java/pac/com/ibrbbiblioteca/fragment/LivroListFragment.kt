package pac.com.ibrbbiblioteca.fragment

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.*
import android.util.Log
import android.view.*
import kotlinx.android.synthetic.main.fragment_livro_list.*
import pac.com.ibrbbiblioteca.R

import pac.com.ibrbbiblioteca.adapter.LivroAdapter
import pac.com.ibrbbiblioteca.model.Livro
import pac.com.ibrbbiblioteca.restapi.BibliotecaAPI
import pac.com.ibrbbiblioteca.restapi.RetrofitInit
import pac.com.ibrbbiblioteca.util.JwtUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by alexandre on 23/04/18.
 */
class LivroListFragment : android.app.Fragment()
        , LivroAdapter.OnBookSelected
        , LivroAdapter.OnMarkFavoriteBook {


    private var mLivros: ArrayList<Livro> = ArrayList<Livro>()
    private var mAdapter: LivroAdapter? = null
    private var filtroAdapter: LivroAdapter? = null
    public var chamada : Call<List<Livro>>? = null

    override fun onCreateView(inflater: LayoutInflater?
                              , container: ViewGroup?
                              , savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_livro_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        var booksPerRow: Int = this.activity.resources.getInteger(R.integer.books_per_row)
        recViewBooks.layoutManager = GridLayoutManager(this.activity, booksPerRow)

        if(chamada==null) {
            chamada = RetrofitInit()
                    .bibliotecaApi()
                    .getLivros(token = JwtUtil().getJwt(this.activity))
        }
        if (chamada?.isExecuted == false) {
            exibirProgresso(true)
            chamada?.enqueue(object : Callback<List<Livro>?> {
                override fun onResponse(call: Call<List<Livro>?>?, response: Response<List<Livro>?>?) {
                    if (response?.code() == 200) {
                        response?.body()?.let {
                            var livros: ArrayList<Livro> = ArrayList<Livro>(it)
                            configurarRecycler(livros)
                        }
                    } else{
                        var v: View = recViewBooks
                        Log.i(this.javaClass.canonicalName, response?.code().toString())
                        Snackbar.make(v, R.string.unable_to_retrieve_books, Snackbar.LENGTH_LONG).show();
                    }
                    exibirProgresso(false)
                }

                override fun onFailure(call: Call<List<Livro>?>?, t: Throwable?) {
                    Log.e(this.javaClass.canonicalName, t.toString())
                    var v: View = recViewBooks
                    Snackbar.make(v, R.string.service_unavailable, Snackbar.LENGTH_LONG).show();
                    exibirProgresso(false)
                }

            })
        } else {
            configurarRecycler(mLivros)
        }
    }

    private fun configurarRecycler(livros: ArrayList<Livro>) {
        if (!isVisible)
            return
        mLivros = livros
        mAdapter = LivroAdapter(mLivros,JwtUtil().hasValue(this.activity))
        recViewBooks?.adapter = mAdapter
        mAdapter?.onBookSelected = this
        mAdapter?.onMarkBookFavorite = this
    }

    override fun onMarkFavoriteBooK(livro: Livro, position: Int) {
        var jwt: String = JwtUtil().getJwt(this.activity)

        var api: BibliotecaAPI = RetrofitInit()
                .bibliotecaApi()
        var call: Call<Livro>?
        var hashLivroId: HashMap<String, Object> = HashMap<String, Object>()
        hashLivroId.put("livroId", livro._id as Object)
        if (livro.favorito) {
            call = api.removeLivroFavorito(jwt, hashLivroId)
        } else {
            call = api.addLivroFavorito(jwt, hashLivroId)
        }
        call?.enqueue(object : Callback<Livro?> {
            override fun onFailure(call: Call<Livro?>?, t: Throwable?) {
                if (!isVisible)
                    return
                var v: View = recViewBooks
                Snackbar.make(v, R.string.service_unavailable, Snackbar.LENGTH_LONG).show()
                Log.i(this.javaClass.canonicalName, t?.toString())

            }

            override fun onResponse(call: Call<Livro?>?, response: Response<Livro?>?) {
                if (!isVisible)
                    return
                if (response?.code() == 200) {
                    response?.body()?.let {
                        var livroResult:Livro = it
                        mLivros[position] = livroResult
                        configurarRecycler(mLivros)
                    }
                    var v: View = recViewBooks
                    recViewBooks.adapter.notifyItemChanged(position)
                    Snackbar.make(v, R.string.marked_favorite_not_successfull, Snackbar.LENGTH_LONG).show();
                } else if(response?.code()==403){
                    var v: View = recViewBooks
                    Log.i(this.javaClass.canonicalName, response?.code().toString())
                    Snackbar.make(v, R.string.feature_only_logged, Snackbar.LENGTH_LONG).show();
                } else {
                    var v: View = recViewBooks
                    Snackbar.make(v, R.string.marked_favorite_failure, Snackbar.LENGTH_LONG).show();
                    Log.i(this.javaClass.canonicalName, response?.toString())
                }
            }
        })
    }

    override fun onBookSelected(livro: Livro) {

        var broadcastLivroSelected: String = resources.getString(R.string.book_selected_broadcast)
        var intentLivroEscolhido: Intent = Intent(broadcastLivroSelected)
        intentLivroEscolhido.putExtra(
                resources.getString(R.string.book_selected)
                , livro)
        LocalBroadcastManager
                .getInstance(this.activity)
                .sendBroadcast(intentLivroEscolhido)
    }

    fun exibirProgresso(visible:Boolean){
        fragment_list_livro_progress.visibility = if (visible) View.VISIBLE else View.GONE
        recViewBooks.visibility = if (visible) View.GONE else View.VISIBLE
    }

}