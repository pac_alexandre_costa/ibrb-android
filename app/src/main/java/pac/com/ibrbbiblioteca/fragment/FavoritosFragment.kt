package pac.com.ibrbbiblioteca.fragment

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.fragment_favoritos.*
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.adapter.LivroAdapter
import pac.com.ibrbbiblioteca.model.Livro
import pac.com.ibrbbiblioteca.restapi.RetrofitInit
import pac.com.ibrbbiblioteca.util.JwtUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by alexandre on 07/05/18.
 */
class FavoritosFragment : android.app.Fragment(),
        View.OnClickListener{
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_favoritos, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(!JwtUtil().hasValue(this.activity)){
            favoritosContainer.visibility = View.GONE
            return
        }

        recViewBooksFavoritos.layoutManager = LinearLayoutManager(this.activity, LinearLayoutManager.HORIZONTAL,false)
        var call = RetrofitInit().bibliotecaApi().getLivrosFavoritos(JwtUtil().getJwt(this.activity))
        call.enqueue(object : Callback<List<Livro>?> {
            override fun onFailure(call: Call<List<Livro>?>?
                                   , t: Throwable?) {
                Log.i(this.javaClass.canonicalName, t.toString())
                var v:View =recViewBooksFavoritos
                Snackbar.make(v, R.string.service_unavailable, Snackbar.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<Livro>?>?
                                    , response: Response<List<Livro>?>?) {
                if(response?.code()==200){
                    response?.body()?.let{
                        var livrosTop10 : List<Livro> = it
                        configurarRecycler(livrosTop10)
                    }
                }else{
                    Log.i(this.javaClass.canonicalName, response.toString())

                }
            }

        })

    }

    fun configurarRecycler(livrosTop10:List<Livro>){
        if (!isVisible)
            return
        recViewBooksFavoritos.adapter = LivroAdapter(livrosTop10,false)
    }

    override fun onClick(view: View?) {
        view?.let{
            var position : Int = recViewBooksFavoritos.getChildLayoutPosition(it)
            var adapter : LivroAdapter = recViewBooksFavoritos.adapter as LivroAdapter
            var livroEscolhido : Livro = adapter.livroList.get(position)

            var broadcastLivroSelected : String = resources.getString(R.string.book_selected_broadcast)
            var intentLivroEscolhido : Intent = Intent(broadcastLivroSelected)
            intentLivroEscolhido.putExtra(
                    resources.getString(R.string.book_selected)
                    , livroEscolhido)
            LocalBroadcastManager
                    .getInstance(this.activity)
                    .sendBroadcast(intentLivroEscolhido)
        }


    }
}