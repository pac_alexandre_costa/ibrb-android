package pac.com.ibrbbiblioteca.fragment

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import pac.com.ibrbbiblioteca.R

import kotlinx.android.synthetic.main.fragment_autor_list.*
import pac.com.ibrbbiblioteca.adapter.AutorAdapter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import kotlinx.android.synthetic.main.activity_home.*
import pac.com.ibrbbiblioteca.adapter.LivroAdapter
import pac.com.ibrbbiblioteca.model.Autor
import pac.com.ibrbbiblioteca.model.Livro
import pac.com.ibrbbiblioteca.restapi.RetrofitInit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by alexandre on 26/04/18.
 */
class AutorListFragment : android.app.Fragment()
        , AutorAdapter.OnAuthorSelected {

    private var mAutores: List<Autor> = ArrayList<Autor>()
    private var mAdapter: AutorAdapter? = null
    private var filtroAdapter : AutorAdapter? = null
    private var autorAdapter: AutorAdapter? = null
    var autorSelectedListener : AutorAdapter.OnAuthorSelected? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater?
                              , container: ViewGroup?
                              , savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_autor_list, container, false)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        recViewAuthors.layoutManager = LinearLayoutManager(this.activity)
        recViewAuthors.addItemDecoration(DividerItemDecoration(this.activity
                , DividerItemDecoration.VERTICAL))

        var call = RetrofitInit().bibliotecaApi().getAutores()
        call.enqueue(object: Callback<List<Autor>?> {
            override fun onResponse(call: Call<List<Autor>?>?
                                    , response: Response<List<Autor>?>?) {

                if(response?.code()==200){
                    response?.body()?.let{
                        var autores : List<Autor> = it
                        configurarRecycler(autores)
                    }
                }else{
                    Log.e(this.javaClass.canonicalName, response?.toString())
                    var v: View = recViewAuthors
                    Snackbar.make(v, R.string.unable_to_retrieve_authors, Snackbar.LENGTH_LONG).show()
                }

            }

            override fun onFailure(call: Call<List<Autor>?>?
                                   , t: Throwable?) {
                Log.e(this.javaClass.canonicalName, t.toString())
                var v: View = recViewAuthors
                Snackbar.make(v, R.string.service_unavailable, Snackbar.LENGTH_LONG).show()
            }

        })


    }

    private fun configurarRecycler(autores: List<Autor>) {
        if (!isVisible)
            return


        mAutores = autores
        mAdapter = AutorAdapter(mAutores)
        mAdapter?.onAuthorSelected = this
        recViewAuthors?.adapter = mAdapter
    }

    override fun onAuthorSelected(autor: Autor) {
        var broadcastAutorSelected: String = resources.getString(R.string.author_selected_broadcast)
        var intentAutorEscolhido: Intent = Intent(broadcastAutorSelected)
        intentAutorEscolhido.putExtra(
                resources.getString(R.string.author_selected)
                , autor)
        LocalBroadcastManager
                .getInstance(this.activity)
                .sendBroadcast(intentAutorEscolhido)
    }


}