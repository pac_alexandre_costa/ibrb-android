package pac.com.ibrbbiblioteca.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pac.com.ibrbbiblioteca.R

import kotlinx.android.synthetic.main.fragment_top10.*
import pac.com.ibrbbiblioteca.adapter.LivroAdapter
import pac.com.ibrbbiblioteca.model.Livro
import pac.com.ibrbbiblioteca.restapi.RetrofitInit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by alexandre on 07/05/18.
 */
class Top10Fragment : android.app.Fragment()
        , LivroAdapter.OnBookSelected
{
    private var top10Livros : List<Livro> = ArrayList<Livro>()
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_top10, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recViewBooksTop10.layoutManager = LinearLayoutManager(this.activity, LinearLayoutManager.HORIZONTAL,false)
        if (top10Livros.size>0){
            configurarRecycler(top10Livros)
        }else {
            var call = RetrofitInit().bibliotecaApi().getTop10()
            call.enqueue(object : Callback<List<Livro>?> {
                override fun onFailure(call: Call<List<Livro>?>?
                                       , t: Throwable?) {
                    Log.i(this.javaClass.canonicalName, t.toString())
                }

                override fun onResponse(call: Call<List<Livro>?>?
                                        , response: Response<List<Livro>?>?) {
                    if (response?.code() == 200) {
                        response?.body()?.let {
                            var livrosTop10: List<Livro> = it
                            configurarRecycler(livrosTop10)
                        }
                    } else {
                        Log.i(this.javaClass.canonicalName, response.toString())

                    }
                }
            })
        }
    }

    fun configurarRecycler(livrosTop10:List<Livro>){
        if (! isVisible)
            return
        top10Livros = livrosTop10
        var _adapter = LivroAdapter(livrosTop10,false)
        _adapter.onBookSelected = this
        recViewBooksTop10.adapter = _adapter
    }

    override fun onBookSelected(livro: Livro) {

        var broadcastLivroSelected: String = resources.getString(R.string.book_selected_broadcast)
        var intentLivroEscolhido: Intent = Intent(broadcastLivroSelected)
        intentLivroEscolhido.putExtra(
                resources.getString(R.string.book_selected)
                , livro)
        LocalBroadcastManager
                .getInstance(this.activity)
                .sendBroadcast(intentLivroEscolhido)
    }
}