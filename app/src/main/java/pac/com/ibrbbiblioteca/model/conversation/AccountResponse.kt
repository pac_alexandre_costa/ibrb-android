package pac.com.ibrbbiblioteca.model.conversation

/**
 * Created by alexandre on 23/04/18.
 */
data class AccountResponse(val success:String, val message:String, val token:String)