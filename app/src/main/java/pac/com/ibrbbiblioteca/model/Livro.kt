package pac.com.ibrbbiblioteca.model

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by alexandre on 23/04/18.
 */
data class Livro(val _id: String
                 , val titulo: String
                 , val isbn: String
                 , val ano: Int
                 , val descricao: String
                 , val capa: String
                 , val imagem : String
                 , val autor: List<Autor>
                 , val favorito:Boolean)
    : Serializable{

    var bitmapCapa : Bitmap?=null

    fun autores(): String {
        var autores = StringBuilder()
        for (autor: Autor in autor)
            autores.append(autor.nome).append(", ")
        return autores.substring(0, autores.toString().length - 2)

    }

    fun contem(query: String): Boolean {
        if (titulo.toLowerCase().contains(query.toLowerCase())) {
            return true
        } else {
            for(autor:Autor in autor){
                if(autor.nome.toLowerCase().contains(query.toLowerCase()))
                    return true
            }
        }
        return false
    }
}