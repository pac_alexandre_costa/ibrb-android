package pac.com.ibrbbiblioteca.model.conversation

/**
 * Created by alexandre on 23/04/18.
 */
data class AccountRequest(val nome:String, val email:String, val password:String)