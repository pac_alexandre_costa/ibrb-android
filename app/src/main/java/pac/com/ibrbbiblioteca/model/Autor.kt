package pac.com.ibrbbiblioteca.model

import java.io.Serializable

/**
 * Created by alexandre on 23/04/18.
 */
data class Autor(val _id:String
                 , val nome:String):Serializable