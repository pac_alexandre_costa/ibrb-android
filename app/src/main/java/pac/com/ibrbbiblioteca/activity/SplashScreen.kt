package pac.com.ibrbbiblioteca.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_splashscreen.*
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.util.JwtUtil

/**
 * Created by alexandre on 04/06/18.
 */
class SplashScreen : AppCompatActivity()
        , JwtUtil.JwtObtained {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        var pulse: Animation = AnimationUtils.loadAnimation(this, R.anim.pulse)
        img_app_pulse.startAnimation(pulse)
        JwtUtil().obtainJwt(this, this)
    }

    override fun onJwtObtained(jwtToken: String?) {
        var intent : Intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        this.finish()
    }

}