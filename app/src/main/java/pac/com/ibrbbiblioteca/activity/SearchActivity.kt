package pac.com.ibrbbiblioteca.activity

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

/**
 * Created by paulo on 02/05/2018.
 */
class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var intent:Intent = getIntent()
        if(Intent.ACTION_SEARCH.equals(intent.action)){
            var query:String = intent.getStringExtra(SearchManager.QUERY)
            Log.i(this.javaClass.canonicalName, "Query Recebida: "+query)
        }
    }
}