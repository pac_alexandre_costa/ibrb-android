package pac.com.ibrbbiblioteca.activity

import android.app.Fragment
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.model.Livro
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.GravityCompat
import android.support.v7.widget.SearchView
import android.view.Menu
import kotlinx.android.synthetic.main.activity_home.*
import pac.com.ibrbbiblioteca.fragment.*
import pac.com.ibrbbiblioteca.model.Autor
import pac.com.ibrbbiblioteca.restapi.RetrofitInit
import pac.com.ibrbbiblioteca.util.JwtUtil
import pac.com.ibrbbiblioteca.util.UserUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by alexandre on 23/04/18.
 */
class HomeActivity : AppCompatActivity()
        , NavigationView.OnNavigationItemSelectedListener
        , SearchView.OnQueryTextListener {

    var broadcastEventListener: BroadcastListener = BroadcastListener()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        nav_view.setNavigationItemSelectedListener(this)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu)

        if (savedInstanceState == null) {
            configurarHomeFragment()
        }

        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(broadcastEventListener
                        , IntentFilter(getString(R.string.book_selected_broadcast)))

        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(broadcastEventListener
                        , IntentFilter(getString(R.string.book_marked_as_favorite)))

        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(broadcastEventListener
                        , IntentFilter(getString(R.string.book_unmarked_as_favorite)))
        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(broadcastEventListener
                        , IntentFilter(getString(R.string.author_selected_broadcast)))
        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(broadcastEventListener
                        , IntentFilter(getString(R.string.login_broadcast)))

        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(broadcastEventListener
                        , IntentFilter(getString(R.string.logout_broadcast)))

        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(broadcastEventListener
                        , IntentFilter(getString(R.string.signup_request_broadcast)))

        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(broadcastEventListener
                        ,IntentFilter(getString(R.string.see_books_author_broadcast)))



        configurarVisibilidadeLoginLogff(!JwtUtil().hasValue(this))

    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_home -> {
                configurarHomeFragment()
            }
            R.id.menu_item_book -> {
                configurarFragmentLivroList()
            }
            R.id.menu_item_authors -> {
                configurarFragmentAutorList()
            }
            R.id.menu_item_log_on -> {
                configurarFragmentLogin()
            }
            R.id.menu_item_log_off -> {
                configurarFragmentLogout()
            }
            R.id.menu_item_about -> {
                configurarFragmentAbout()
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.options_menu, menu)
        var searchItem: MenuItem? = menu?.findItem(R.id.action_search)
        var searchView: SearchView? = searchItem?.actionView as SearchView?
        searchView?.queryHint = resources.getString(R.string.action_search)
        searchView?.setOnQueryTextListener(this)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Log.i(this.javaClass.canonicalName, "item selected " + item.itemId)
        when (item.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun configurarHomeFragment() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, HomeFragment())
                .addToBackStack(null)
                .commit()
    }

    private fun configurarFragmentLivroList() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, LivroListFragment())
                .addToBackStack(null)
                .commit()
    }

    private fun configurarFragmentLivroSearch(queryStr: String?) {
        var frag = LivroListFragment()

        var call = RetrofitInit()
                .bibliotecaApi()
                .getLivros(queryStr = queryStr, token = JwtUtil().getJwt(this))
        frag.chamada = call
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, frag)
                .addToBackStack(null)
                .commit()
    }

    private fun configurarFragmentLivroAutor(autor:Autor) {
        var frag = LivroListFragment()

        var call = RetrofitInit()
                .bibliotecaApi()
                .getLivrosAutor(autor._id, JwtUtil().getJwt(this))
        frag.chamada = call
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, frag)
                .addToBackStack(null)
                .commit()
    }

    private fun configurarFragmentLivro(livro: Livro) {
        var livroFragment: LivroFragment = LivroFragment()
        livroFragment.livro = livro
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, livroFragment)
                .addToBackStack(null)
                .commit()
    }


    private fun configurarFragmentAutorList() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, AutorListFragment())
                .addToBackStack(null)
                .commit()
    }

    private fun configurarFragmentAutor(autor: Autor) {
        var autorFragment: AutorFragment = AutorFragment()
        autorFragment.autor = autor
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, autorFragment)
                .addToBackStack(null)
                .commit()
    }

    private fun configurarFragmentLogin() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, LoginFragment())
                .addToBackStack(null)
                .commit()
    }

    private fun configurarFragmentLogout() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, LogoutFragment())
                .addToBackStack(null)
                .commit()
    }

    private fun configurarFragmentSignUp() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, SignUpFragment())
                .addToBackStack(null)
                .commit()
    }

    private fun configurarFragmentAbout() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, AboutFragment())
                .addToBackStack(null)
                .commit()
    }


    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager
                .getInstance(this)
                .unregisterReceiver(broadcastEventListener)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        configurarFragmentLivroSearch(query)
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }

    fun configurarVisibilidadeLoginLogff(showLogin: Boolean) {
        nav_view.menu.findItem(R.id.menu_item_log_on).setVisible(showLogin)
        nav_view.menu.findItem(R.id.menu_item_log_off).setVisible(!showLogin)
    }

    inner class BroadcastListener : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                when (it.action) {
                    resources.getString(R.string.book_selected_broadcast) -> {
                        var field: String = resources.getString(R.string.book_selected)
                        var livro: Livro = it.getSerializableExtra(field) as Livro
                        configurarFragmentLivro(livro)
                    }
                    resources.getString(R.string.book_marked_as_favorite) -> {

                    }
                    resources.getString(R.string.book_unmarked_as_favorite) -> {

                    }
                    resources.getString(R.string.author_selected_broadcast) -> {
                        var field: String = resources.getString(R.string.author_selected)
                        var autor: Autor = it.getSerializableExtra(field) as Autor
                        configurarFragmentAutor(autor)
                    }
                    resources.getString(R.string.login_broadcast) -> {
                        configurarHomeFragment()
                        configurarVisibilidadeLoginLogff(false)
                    }
                    resources.getString(R.string.logout_broadcast) -> {
                        configurarHomeFragment()
                        JwtUtil().clearJwt(this@HomeActivity)
                        UserUtil().clearEmailPassword(this@HomeActivity)
                        configurarVisibilidadeLoginLogff(true)
                    }
                    resources.getString(R.string.signup_request_broadcast) -> {
                        configurarFragmentSignUp()
                        JwtUtil().clearJwt(this@HomeActivity)
                        UserUtil().clearEmailPassword(this@HomeActivity)
                    }
                    resources.getString(R.string.see_books_author_broadcast)->{
                        var field: String = resources.getString(R.string.author_selected)
                        var autor: Autor = it.getSerializableExtra(field) as Autor
                        configurarFragmentLivroAutor(autor)
                    }
                    else -> {

                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        var currentFragment: Fragment? = fragmentManager.findFragmentById(R.id.content_frame)

        if (currentFragment is HomeFragment) {
            this.finish()
        } else {
            super.onBackPressed()
        }
    }

}