package pac.com.ibrbbiblioteca.adapter

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.model.Livro

import kotlinx.android.synthetic.main.livro_card.view.*
import pac.com.ibrbbiblioteca.util.ImageUtil

/**
 * Created by alexandre on 23/04/18.
 */
class LivroAdapter(var livroList: List<Livro>, var showFavoriteIcon:Boolean) :
        RecyclerView.Adapter<LivroAdapter.LivroViewHolder>() {

    var onMarkBookFavorite: OnMarkFavoriteBook? = null
    var onBookSelected: OnBookSelected? = null


    override fun onBindViewHolder(holder: LivroViewHolder?, position: Int) {
        holder?.setFields(livroList.get(position))
        holder
                ?.itemView
                ?.item_list_favorito
                ?.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(view: View?) {
                        onMarkBookFavorite
                                ?.onMarkFavoriteBooK(livroList.get(position),position)
                    }
                })
        holder
                ?.itemView
                ?.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(view: View?) {
                        onBookSelected
                                ?.onBookSelected(livroList.get(position))
                    }
                })
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): LivroViewHolder {
        var layoutInflater: LayoutInflater = LayoutInflater.from(parent?.context)
        var v: View = layoutInflater.inflate(R.layout.livro_card, parent, false)
        return LivroViewHolder(v,showFavoriteIcon)
    }

    override fun getItemCount(): Int {
        return livroList.size
    }

    interface OnMarkFavoriteBook {
        fun onMarkFavoriteBooK(livro: Livro, position:Int)
    }

    interface OnBookSelected {
        fun onBookSelected(livro: Livro)
    }

    class LivroViewHolder(itemView: View, var showFavoriteIcon: Boolean) : RecyclerView.ViewHolder(itemView) {

        var livro: Livro? = null
        val titulo = itemView.item_list_livro_titulo
        val capa = itemView.item_list_livro_capa
        var favorito = itemView.item_list_favorito

        fun setFields(livro: Livro) {
            this.livro = livro
            titulo.text = livro.titulo
            capa.setImageResource(R.drawable.ic_book)

            if(showFavoriteIcon) {
                favorito.visibility = View.VISIBLE
                favorito.setImageResource(R.drawable.ic_not_favorite)
                if (livro.favorito) {
                    favorito.setImageResource(R.drawable.ic_favorite)
                }
            }


            livro.imagem?.let {
                if (it.isNotEmpty() && it.isNotBlank()) {
                    try {
                        var decodedByte: Bitmap = ImageUtil().toBitmap(it)
                        capa.setImageBitmap(decodedByte)
                    } catch (e: Exception) {
                        Log.i(this.javaClass.canonicalName, e.toString())
                    }
                }
            }
        }
    }


}