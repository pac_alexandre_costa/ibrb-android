package pac.com.ibrbbiblioteca.adapter


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pac.com.ibrbbiblioteca.model.Autor

import kotlinx.android.synthetic.main.fragment_autor_list_item.view.*
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.model.Livro

/**
 * Created by alexandre on 26/04/18.
 */
class AutorAdapter (var autorList:List<Autor>)   :
        RecyclerView.Adapter<AutorAdapter.AutorViewHolder>(){

    var onAuthorSelected : OnAuthorSelected?=null
    override fun onBindViewHolder(holder: AutorViewHolder?, position: Int) {
        holder?.setFields(autorList.get(position))
        holder
                ?.itemView
                ?.setOnClickListener(object:View.OnClickListener{
                    override fun onClick(view: View?) {
                        onAuthorSelected
                                ?.onAuthorSelected(autorList.get(position))
                    }
                })
    }


    override fun getItemCount(): Int {
        return autorList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AutorViewHolder {
        var layoutInflater: LayoutInflater = LayoutInflater.from(parent?.context)
        var v : View = layoutInflater.inflate(R.layout.fragment_autor_list_item,parent,false)
        return AutorViewHolder(v)
    }


    class AutorViewHolder(item_view: View) : RecyclerView.ViewHolder(item_view) {
        val nome = item_view.item_list_autor_nome

        fun setFields(autor:Autor){
            nome.text = autor.nome
        }
    }

    interface OnAuthorSelected {
        fun onAuthorSelected(autor: Autor)
    }

}