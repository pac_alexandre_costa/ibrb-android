package pac.com.ibrbbiblioteca.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64

/**
 * Created by alexandre on 21/05/18.
 */
class ImageUtil {
    fun toBitmap(imageBase64:String): Bitmap {
        var decodedString: ByteArray = Base64.decode(imageBase64, Base64.DEFAULT)
        var decodedByte: Bitmap =
                BitmapFactory.decodeByteArray(decodedString
                        , 0
                        , decodedString.size)
        return decodedByte
    }
}