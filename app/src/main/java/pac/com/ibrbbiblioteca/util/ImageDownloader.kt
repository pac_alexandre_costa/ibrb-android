package pac.com.ibrbbiblioteca.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.Image
import android.os.AsyncTask
import android.util.Base64
import android.util.Log
import android.widget.ImageView
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.model.Livro
import java.io.InputStream
import java.net.MalformedURLException

/**
 * Created by alexandre on 03/05/18.
 */
class ImageDownloader(var imageView: ImageView , var livro: Livro) : AsyncTask<Void,Void, Bitmap>() {
    override fun doInBackground(vararg p0: Void?): Bitmap? {
        var bitmap : Bitmap? =null

        try {
            livro.bitmapCapa?.let{
                bitmap = it
            } ?: run {
                var input: InputStream = java.net.URL(livro.capa).openStream()
                bitmap = BitmapFactory.decodeStream(input)
            }

        }catch(malFormedUrl:MalformedURLException){
            Log.e(this.javaClass.canonicalName, "MalFormedUrl "+livro.capa)
        }

        return bitmap
    }

    override fun onPostExecute(result: Bitmap?) {
        result?.let{
            imageView.setImageBitmap(it)
            livro.bitmapCapa = it
        } ?: run{
            imageView.setImageResource(R.drawable.ic_book)
            livro.bitmapCapa=null
        }
    }
}