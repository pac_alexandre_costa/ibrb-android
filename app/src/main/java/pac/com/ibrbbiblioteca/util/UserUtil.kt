package pac.com.ibrbbiblioteca.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import pac.com.ibrbbiblioteca.R

/**
 * Created by paulo on 15/05/2018.
 */
class UserUtil {

    public fun storeEmailPassword(context: Context, user:String, password:String){
        var sharedPrefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        var editor: SharedPreferences.Editor = sharedPrefs.edit()
        editor.putString(context.getString(R.string.shared_pref_user), user)
        editor.putString(context.getString(R.string.shared_pref_password), password)
        editor.commit()
    }

    public fun clearEmailPassword(context:Context){
        storeEmailPassword(context,"","")
    }

    public fun getEmail(context:Context):String{
        var sharedPrefs:SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPrefs.getString(context.getString(R.string.shared_pref_user),"")

    }
    public fun getPassword(context:Context):String{
        var sharedPrefs:SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPrefs.getString(context.getString(R.string.shared_pref_password),"")
    }



}