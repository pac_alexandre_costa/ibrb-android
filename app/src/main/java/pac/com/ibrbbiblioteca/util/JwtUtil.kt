package pac.com.ibrbbiblioteca.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import pac.com.ibrbbiblioteca.R
import pac.com.ibrbbiblioteca.model.conversation.AccountRequest
import pac.com.ibrbbiblioteca.model.conversation.AccountResponse
import pac.com.ibrbbiblioteca.restapi.RetrofitInit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by alexandre on 23/04/18.
 */
class JwtUtil {

    public fun setJwt(jwt:String, context: Context){
        var sharedPrefs:SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        var editor:SharedPreferences.Editor = sharedPrefs.edit()
        editor.putString(context.getString(R.string.jwt), jwt)
        editor.commit()
    }

    public fun clearJwt(context:Context){
        setJwt("", context)
    }

    public fun getJwt(context:Context):String{
        var sharedPrefs:SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPrefs.getString(context.getString(R.string.jwt),"")
    }

    public fun hasValue(context:Context):Boolean{
        var jwt : String = getJwt(context)
        return jwt.isNotBlank()&&jwt.isNotEmpty()
    }



    public fun obtainJwt(context:Context, onJwtObtained: JwtObtained){
        var email:String = UserUtil().getEmail(context)
        var password:String = UserUtil().getPassword(context)

        var accountRequest:AccountRequest = AccountRequest("",email,password)
        var call = RetrofitInit()
                .bibliotecaApi()
                .login(accountRequest)
        call.enqueue(object: Callback<AccountResponse> {
            override fun onFailure(call: Call<AccountResponse>?, t: Throwable?) {
                onJwtObtained.onJwtObtained(null)
                Log.i(this.javaClass.canonicalName,t.toString())
            }

            override fun onResponse(call: Call<AccountResponse>?, response: Response<AccountResponse>?) {
                if(response?.code()==200){
                    var accountResponse:AccountResponse = response?.body() as AccountResponse
                    JwtUtil().setJwt(accountResponse.token, context)
                    onJwtObtained.onJwtObtained(accountResponse.token)

                }else{
                    onJwtObtained.onJwtObtained(null)
                    Log.i(this.javaClass.canonicalName, response?.toString())
                }
            }
        })


    }

    public interface JwtObtained{
        public fun onJwtObtained(jwtToken:String?)
    }
}