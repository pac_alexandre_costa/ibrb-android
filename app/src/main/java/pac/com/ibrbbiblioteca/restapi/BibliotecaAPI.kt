package pac.com.ibrbbiblioteca.restapi

import okhttp3.ResponseBody
import pac.com.ibrbbiblioteca.model.Autor
import pac.com.ibrbbiblioteca.model.Livro
import pac.com.ibrbbiblioteca.model.conversation.AccountRequest
import pac.com.ibrbbiblioteca.model.conversation.AccountResponse
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by alexandre on 23/04/18.
 */
interface BibliotecaAPI {

    @POST("account/login") fun login(@Body loginRequest : AccountRequest):Call<AccountResponse>
    @POST("account/signup") fun signup(@Body loginRequest : AccountRequest):Call<AccountResponse>
    @GET("livro/top10") fun getTop10() : Call<List<Livro>>
    @GET("user/favoritos") fun getLivrosFavoritos(@Header("x-access-token") token:String) : Call<List<Livro>>
    @PUT("user/favoritos/add") fun addLivroFavorito(@Header("x-access-token") token:String, @Body livroId:HashMap<String,Object>) : Call<Livro>
    @PUT("user/favoritos/remove") fun removeLivroFavorito(@Header("x-access-token") token:String, @Body livroId:HashMap<String,Object>) : Call<Livro>
    @GET("livro") fun getLivros(@Query("queryStr") queryStr:String?="",  @Header("x-access-token") token:String="") : Call<List<Livro>>
    @GET("autor") fun getAutores( @Header("x-access-token") token:String="") : Call<List<Autor>>
    @GET("autor/{autor_id}/livros") fun getLivrosAutor( @Path("autor_id") autorId:String, @Header("x-access-token") token:String="") : Call<List<Livro>>

}