package pac.com.ibrbbiblioteca.restapi

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by alexandre on 23/04/18.
 */
class RetrofitInit {
    val BASE_URL = "https://shielded-meadow-49313.herokuapp.com/api/v1/"
    var gson:Gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()

    private val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    fun bibliotecaApi() = retrofit.create(BibliotecaAPI::class.java)
}